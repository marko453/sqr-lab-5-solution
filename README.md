# Lab 5 - Integration Testing

Student name: <b>Marko Pezer</b><br>
Student email: <b>m.pezer@innopolis.university</b><br>
Student group: <b>BS18-SE-01</b>

## Specifications

Here is my list of specifications that I got from provided API: <br>

```
Budet car price per minute = 16
Luxury car price per minute = 38
Fixed price per km = 15
Allowed deviations in % = 10
Inno discount in % = 9
```

## Boundary Value Analysis (BVA)

Here is the list of all possible parameters that we can have: <br>

| Parameter          | Values                              |
|--------------------|-------------------------------------|
| `plan`             | `minute`; `fixed_price`; `nonsense` |
| `type`             | `budget`; `luxury`; `nonsense`      |
| `distance`         | `< = 0`; `> 0`                      |
| `planned_distance` | `< = 0`; `> 0`                      |
| `time`             | `< = 0`; `> 0`                      |
| `planned_time`     | `< = 0`; `> 0`                      |
| `inno_discount`    | `yes`; `no`; `nonsense`             |


## Decision table

Here is decision table: <br>

| Parameter           | Values                                |  R1    |  R2   |  R3   |  R4   |     R5     |     R6     |     R7     |      R8       |    R9    |   R10    |      R11      |      R12      |   R13    |   R14    |
| ------------------- | ------------------------------------- |:------:|:-----:|:-----:|:-----:|:----------:|:----------:|:----------:|:-------------:|:--------:|:--------:|:-------------:|:-------------:|:--------:|:--------:|
| `distance`          | `> 0`; `<= 0`                         | `<= 0` | ` > 0`  | `> 0`  | `> 0`  |    `> 0`    |    `> 0`    |    `> 0`    |     `> 0`      |   `> 0`   |   `> 0`   |     `> 0`      |     `> 0`      |   `> 0`   |   `> 0`   |
| `planned_distance`  | `> 0`; `<= 0`                         |   *    | `<= 0` | `> 0`  | `> 0`  |    `> 0`    |    `> 0`    |    `> 0`    |     `> 0`      |   `> 0`   |   `> 0`   |     `> 0`      |     `> 0`      |   `> 0`   |   `> 0`   |
| `time`              | `> 0`; `<= 0`                         |   *    |   *   | `<= 0` | `> 0`  |    `> 0`    |    `> 0`    |    `> 0`    |     `> 0`      |   `> 0`   |   `> 0`   |     `> 0`      |     `> 0`      |   `> 0`   |   `> 0`   |
| `planned_time`      | `> 0`; `<= 0`                         |   *    |   *   |   *   | `<= 0` |    `> 0`    |    `> 0`    |    `> 0`    |     `> 0`      |   `> 0`   |   `> 0`   |     `> 0`      |     `> 0`      |   `> 0`   |   `> 0`   |
| `type`              | `budget`, `luxury`, `nonsense`        |   *    |   *   |   *   |   *   | `nonsense` |     *      |     *      |   `luxury`    | `budget` | `budget` |   `budget`    |   `budget`    | `luxury` | `luxury` |
| `plan`              | `minute`, `fixed_price`, `nonsense`   |   *    |   *   |   *   |   *   |     *      | `nonsense` |     *      | `fixed_price` | `minute` | `minute` | `fixed_price` | `fixed_price` | `minute` | `minute` |
| `inno_discount`     | `yes`, `no`, `nonsense`               |   *    |    *   |   *   |   *   |     *      |     *      | `nonsense` |       *       |  `no`    |  `yes`   |     `no`      |     `yes`     |   `no`   |  `yes`   |
| Invalid Request     |                                       |   +    |   +   |   +   |   +   |     +      |     +      |     +      |       +       |          |          |               |               |          |          |
| 200                 |                                       |        |       |       |       |            |            |            |               |    +     |    +     |       +       |       +       |    +     |    +     |

## Test failures

Here is the list of bugs: <br>

`Expected "34.58" instead of "38" for TestCase(type='luxury', plan='minute', distance=1, planned_distance=1, time=1, planned_time=1, inno_discount='yes')`

In this test case, InnoDiscount (which is 9% in my case) is not substracted from price. Therefore, the final price is incorrect. <br>

`Expected "Invalid Request" instead of "38" for TestCase(type='luxury', plan='minute', distance=1, planned_distance=-1, time=1, planned_time=1, inno_discount='no')`

In this test case, we should have "Invalid Request" because planned distance has negative value (in this case, it is -1). However, returned result is 38 which is incorrect.
